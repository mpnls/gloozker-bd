#include <string.h>
#include <vector>
#include <ctime>

#ifndef STUDENT_INCLUDED
#define STUDENT_INCLUDED

using namespace std;
 
typedef struct entry {
    
    tm date;
    tm start_time;
    tm end_time;
    
    bool is_first_reception;
    
    string doctor_name;
    string patient_name;
    
} entry;

void launch_create_dialog();

void print_entries(entry* stdnt);

void print_entries();

void save_file();

void load_file();

void free_memory();

#endif