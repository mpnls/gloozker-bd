#define validate(code) if (!code) { cout << "Canceled or invalid data provided. " << endl << endl; return; }

#include <string.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <ctime>

#include "lib/json.hpp"

using json = nlohmann::json;
using namespace std;

#include "schedule.h"
#include "common.h"

vector<entry> entries_vec;

void launch_create_dialog() {
    entry* entry_temp = new entry;
    
    cout << "Creating entry." << endl;
    
    validate( read_date("date",                  &entry_temp->date));
    validate( read_time("start time",            &entry_temp->start_time));
    validate( read_time("end time",              &entry_temp->end_time));
    validate( read_flag("is it first reception", &entry_temp->is_first_reception));
    validate( read_field("doctor's name",        &entry_temp->doctor_name));
    validate( read_field("patient's name",       &entry_temp->patient_name));
    
    bool confirmed;
    
    string in;
    cout << "Is data correct? (y/n): " << accent;
    cin >> in;
    
    cout << white;
    
    confirmed = (in == "y" || in == "yes");
    
    
    if (confirmed) {
        entries_vec.push_back(*entry_temp);
    }
    
    delete(entry_temp);
    
    if(confirmed)
    {
        cout << "Successfully created entry." << endl << endl;
    }
    else
    {
        cout << "Canceled or invalid data provided. " << endl << endl;
    }
}

void print_entries(entry* stdnt) {
    int day  = stdnt->date.tm_mday;
    int mon  = stdnt->date.tm_mon + 1;
    int year = stdnt->date.tm_year + 1900;
    
    int min  = stdnt->start_time.tm_min;
    int hour = stdnt->start_time.tm_hour;
    
    cout << (day < 10 ? "0" : "") << day << "."
         << (mon < 10 ? "0" : "") << mon << "."
         << year << " " << hour << ":" << min;

    min  = stdnt->end_time.tm_min;
    hour = stdnt->end_time.tm_hour;

    cout << " - " << hour << ":" << min << endl;
    
    cout << white << "Is first receprion: " << accent;
    cout << (stdnt->is_first_reception ? "yes" : "no") << endl;
 
    cout << white << "Doctor: " << accent;
    cout << stdnt->doctor_name << endl;
    
    cout << white << "Patient: " << accent;
    cout << stdnt->patient_name << endl;
         
    cout << white;
}

void print_entries() {
    cout << "Printing " << entries_vec.size() << " entry(s):" << endl << endl;
    
    int i = 1;
    for (entry stdnt : entries_vec) {
        cout << i++ << ". ";
        print_entries(&stdnt);
        cout << endl;
    }
}

void save_file() 
{
    cout << white << "File name (path): " << accent;
    string path;
    cin >> path;
    cout << white;
    
    json entry_array_json = json::array();
    
    for (entry stdnt : entries_vec) 
    {
        json entry_json = json::object();
        
        entry_json["doctor_name"]  = stdnt.doctor_name;
        entry_json["patient_name"]   = stdnt.patient_name;
        
        int day  = stdnt.date.tm_mday;
        int mon  = stdnt.date.tm_mon + 1;
        int year = stdnt.date.tm_year + 1900;
        
        entry_json["date"] = 
            (day < 10 ? "0" : "") + to_string(day) + "." +
            (mon < 10 ? "0" : "") + to_string(mon) + "." +
            to_string(year);
        
        int min  = stdnt.start_time.tm_min;
        int hour = stdnt.start_time.tm_hour;
        
        entry_json["start_time"] = to_string(hour) + ":" + to_string(min);
            
        min  = stdnt.end_time.tm_min;
        hour = stdnt.end_time.tm_hour;
        
        entry_json["end_time"] = to_string(hour) + ":" + to_string(min);
        
        entry_json["is_first_reception"] = stdnt.is_first_reception;
        
        entry_array_json.push_back(entry_json);
    }
    
    ofstream file;
    file.open(path);
    
    if (file.is_open())
    {
        file << entry_array_json.dump(4);
        if (!file) 
        {
            cout << "Error writing to file '" << path << "'" << endl;
            return;
        }
        file.close();
        if (!file) 
        {
            cout << "Error writing to file '" << path << "'" << endl;
            return;
        }
        cout << "Exported " << entries_vec.size() << " entry(s)." << endl << endl;
    }
    else
    {
        cout << "Error writing to file '" << path << "'" << endl;
    }
}

void load_file()
{
    cout << white << "File name (path): " << accent;
    string path;
    cin >> path;
    cout << white;
    
    ifstream file;
    file.open(path);
    
    if (!file.is_open())
    {
        cout << "Error reading file '" << path << "'" << endl;
        return;
    }
    
    stringstream buffer;
    buffer << file.rdbuf();
    
    if (!file) 
    {
        cout << "Error reading file '" << path << "'" << endl;
        return;
    }
    
    string json_data(buffer.str());
    
    file.close();
    
    if (!file) 
    {
        cout << "Error reading file '" << path << "'" << endl;
        return;
    }
    
    json entry_list_json = json::parse(json_data);
    
    for (json::iterator it = entry_list_json.begin(); it != entry_list_json.end(); ++it) {
        
        json entry_json = (json)*it;
        entry* entry_temp = new entry;
        
        entry_temp->doctor_name  = entry_json["doctor_name"];
        entry_temp->patient_name = entry_json["patient_name"];
        
        entry_temp->is_first_reception = entry_json["is_first_reception"];
        
        string date = entry_json["date"];
        strptime(date.c_str(), "%d.%m.%Y", &entry_temp->date);
        
        string time_str = entry_json["start_time"];
        strptime(time_str.c_str(), "%H:%M", &entry_temp->start_time);
        
        time_str = entry_json["end_time"];
        strptime(time_str.c_str(), "%H:%M", &entry_temp->end_time);
        
        entries_vec.push_back(*entry_temp);
    }
    
    cout << "Imported " << entry_list_json.size() << " entry(s)." << endl << endl;
}

void free_memory()
{
    entries_vec.clear();
}